//
//  Users.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit
import RealmSwift

// MARK: - UsersItem
struct UsersItem: Codable {
  let data: Users
  let meta: Meta
  
  enum CodingKeys: String, CodingKey {
    case data
    case meta
  }
  
  // MARK: - Meta
  struct Meta: Codable {
    let perPage, total, currentPage: Int
    let lastPage: Int
    let path: String
    let from, to: Int?
    
    enum CodingKeys: String, CodingKey {
      case from
      case perPage = "per_page"
      case total
      case currentPage = "current_page"
      case lastPage = "last_page"
      case path, to
    }
  }

}

class Users: Object, APIModel, Codable {
  var id: Int
  var login, nodeId: String
  var avatarURL, avatarId, url, htmlURL, followersURL, followingURL, gistsURL, starredURL, subscriptionsURL, organizationsURL, reposURL, receivedEventsURL, eventsURL, type: String
  var siteAdmin: Bool
  
  enum CodingKeys: String, CodingKey {
    case id, login, url, type
    case nodeId = "node_id"
    case avatarURL = "avatar_url"
    case avatarId = "gravatar_id"
    case htmlURL = "html_url"
    case followersURL = "followers_url"
    case followingURL = "following_url"
    case gistsURL = "gists_url"
    case starredURL = "starred_url"
    case subscriptionsURL = "subscriptions_url"
    case organizationsURL = "organizations_url"
    case eventsURL = "events_url"
    case reposURL = "repos_url"
    case receivedEventsURL = "received_events_url"
    case siteAdmin = "site_admin"
  }
}


