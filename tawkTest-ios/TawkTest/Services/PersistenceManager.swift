//
//  PersistenceManager.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 OrganizationName. All rights reserved.
//

import Foundation
import RealmSwift

class PersistenceManager: NSObject {
  static let shared = PersistenceManager()
  let realm = try! Realm()
  
  func saveObject<T: Object>(data: [T]) {
    try! self.realm.write {
      data.forEach { [weak self] deal in
        self?.realm.add(deal, update: .all)
      }
    }
  }
  
  func saveObject<T: Object>(data: T) {
    try! self.realm.write {
      self.realm.add(data, update: .all)
    }
  }

  func fetchCaches<T: Object>(object: T.Type) -> [T] {
    try! self.realm.write {
      let cacheObjects = PersistenceManager.shared.realm.objects(T.self)
      return cacheObjects.map { $0 }
    }
  }
  
  func fetchCachesById<T: Object>(object: T.Type, key: String, value: Int) -> [T] {
    try! self.realm.write {
      let cacheObjects = PersistenceManager.shared.realm.objects(T.self).filter("\(key) = \(value)")
      return cacheObjects.map { $0 }
    }
  }
  
  func deleteCache() {
    try! realm.write {
      realm.deleteAll()
    }
  }
}
