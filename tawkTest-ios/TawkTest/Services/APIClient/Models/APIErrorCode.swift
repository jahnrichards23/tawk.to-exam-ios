//
//  APIErrorCode.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

enum APIErrorCode: String, Codable {

  case unknown = "UNKNOWN_ERROR"

  static let `default`: APIErrorCode = .unknown
}
