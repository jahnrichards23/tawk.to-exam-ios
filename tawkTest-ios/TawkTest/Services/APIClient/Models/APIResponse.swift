//
//  APIResponse.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

struct APIResponse {

  let data: Any?

  let message: String?
  let errorCode: APIErrorCode

  let errors: [String: [String]]?
}

extension APIResponse: Decodable {
  enum CodingKeys: String, CodingKey {
    case data, message, errors
    case errorCode = "error_code"
  }

  init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)

    data = (try container.decodeIfPresent(AnyDecodable.self, forKey: .data))?.value
    message = try container.decodeIfPresent(String.self, forKey: .message)
    errors = try container.decodeIfPresent([String: [String]].self, forKey: .errors)
    errorCode = (try? container.decode(APIErrorCode.self, forKey: .errorCode)) ?? .default
  }
}

// MARK: - Helpers

extension APIResponse {
  func decodedValue<T>(forKeyPath: String? = nil, decoder: JSONDecoder? = nil) -> T? where T: Decodable {
    guard var payload = data else { return nil }

    if let keyPath = forKeyPath {
      guard let d = nestedData(keyPath) else { return nil }
      payload = d
    }

    guard JSONSerialization.isValidJSONObject(payload) else {
      debugLog("payload: \(String(describing: payload))")
      guard let val = payload as? T else { return nil }
      return val
    }

    do {
      let json = try JSONSerialization.data(withJSONObject: payload)
      return try (decoder ?? GenericAPIModel.decoder()).decode(T.self, from: json)

    } catch {
      App.shared.recordError(error, info: ["keyPath": forKeyPath ?? "n/a"])
      return nil
    }
  }

  private func nestedData(_ keyPath: String) -> Any? {
    guard let payload = data, !keyPath.isEmpty else { return nil }
    guard let dict = payload as? [String: Any] else { return nil }
    return dict[keyPath: keyPath] as Any
  }
}
