//
//  APIClientType.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

enum APIClientResult<Value> {
  case success(Value)
  case failure(Error)
}

enum HTTPRequestHeaderContentType: String {
  case json = "application/json"
  case urlEncoded = "application/x-www-form-urlencoded"
}

protocol APIClientType: AppService {
  var baseURL: URL { get }


  var accessToken: String? { get set }

  init(baseURL: URL)
}

extension APIClientType {

  func endpointURL(_ resourcePath: String, version: String? = nil) -> URL {
    return baseURL.appendingPathComponent("\(resourcePath)")
  }

  func httpRequestHeaders(
    withAuth: Bool = true,
    contentType: HTTPRequestHeaderContentType = .json
  ) -> [String: String] {
    var headers = [
      "Content-Type": contentType.rawValue
    ]
    if contentType == .json {
      headers["Accept"] = contentType.rawValue
    }
    return headers
  }
}

protocol APIClientFailedRequestInfoType {
  var status: HTTPStatusCode { get }

  var message: String { get }

  var errorCode: APIErrorCode { get }
}

// MARK: - APIClientError

// TODO: Add conformance to CustomNSError.
enum APIClientError: Error {
  case failedRequest(APIClientFailedRequestInfoType)

  case dataNotFound(_ expectedType: Any.Type)

  case unknown
}

extension APIClientError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case let .failedRequest(info):
      return info.message
    case .dataNotFound:
      return S.errorDevDataUnexpected()
    default:
      return S.errorDevUnknown()
    }
  }

  var failureReason: String? {
    switch self {
    case let .failedRequest(info):
      return S.devHttp(String(describing: info.status), info.message)
    case let .dataNotFound(type):
      return S.errorDevNotExpected(String(describing: type))
    default:
      return S.errorDevUnknown()
    }
  }
}
