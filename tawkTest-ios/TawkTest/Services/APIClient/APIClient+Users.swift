//
//  APIClient+Users.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 OrganizationName. All rights reserved.
//

import Foundation
import CoreLocation
import Alamofire

extension APIClient {
  @discardableResult
  func getUsers(_ completion: @escaping APIUsersClosure) -> DataRequest {
    
    return request(
      Endpoint.usersList ,
      method: .get,
      parameters: nil,
      success: { resp in
        completion(resp.decodedValue(), nil)
    }, failure: { error in
      completion(nil, error)
    })
  }
  
//  @discardableResult
//  func postAddBusinessReaction(_ businessId: Int,
//                               reactionId: Int,
//                               completion: @escaping APIClientIsSuccessClosure) -> DataRequest {
//    return request(
//      String(format: Endpoint.businessReaction, businessId),
//      method: .post,
//      parameters: ["reaction_types": [reactionId]],
//      encoding: JSONEncoding.default,
//      headers: httpRequestHeadersJSON(),
//      success: { resp in
//        completion(resp.status == .ok, nil)
//    }, failure: { error in
//      completion(false, error)
//    })
//  }
  
//  func deleteBusinessReaction(_ businessId: Int,
//                              reactionId: Int,
//                              completion: @escaping APIClientIsSuccessClosure) -> DataRequest {
//    return request(
//      String(format: Endpoint.businessReaction, businessId),
//      method: .delete,
//      success: { resp in
//        completion(resp.status == .ok, nil)
//    }, failure: { error in
//      completion(false, error)
//    })
//  }
//
//  func uploadPhotos(_ photo: UIImage,
//                    businessId: Int,
//                    uploadProgress: @escaping APIClientUploadProgress,
//                    completion: @escaping APIClientMessageErrorClosure) {
//    uploadSinglePhoto(image: photo, businessId: businessId) { _, error in
//      if error == nil {
//          completion(S.businessPhotosUploaded(), nil)
//      } else {
//        completion("", error)
//      }
//    }
//  }
  
//  private func uploadSinglePhoto(image: UIImage, businessId: Int, completion: @escaping APIClientMessageErrorClosure) {
//    Alamofire.upload(
//      multipartFormData: { multipartFormData in
//        if let imageData = image.jpegData(compressionQuality: 0.75) {
//          multipartFormData.append(imageData, withName: "gallery", fileName: "image.png", mimeType: "image/png")
//        }
//    },
//      usingThreshold: UInt64(),
//      to: endpointURL(String(format: Endpoint.businessGallery, businessId)),
//      method: .post,
//      headers: httpRequestHeaders(),
//      encodingCompletion: { encodingResult in
//        switch encodingResult {
//        case .success(let upload, _, _):
//          upload.apiResponse(completion: { result in
//            switch result {
//            case let .success(resp):
//              completion(resp.message ?? "", nil)
//            case let .failure(error):
//              completion("", error)
//            }
//          })
//        case let .failure(error):
//          completion("", error)
//        }
//    }
//    )
//  }
}
