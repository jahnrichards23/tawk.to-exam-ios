//
//  APIClient.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Alamofire
import Foundation
import Valet

typealias APIClientResultClosure = (APIClientResult<APIResponse>) -> Void
typealias APIClientBoolMessageClosure = (Bool, _ message: String?) -> Void
typealias APIUsersClosure = (Users?, Error?) -> Void

struct APIClientFailedRequestInfo: APIClientFailedRequestInfoType {
  let status: HTTPStatusCode
  let message: String
  let errorCode: APIErrorCode
}

class APIClient: APIClientType {
  var baseURL: URL

  required init(baseURL: URL) {
    self.baseURL = baseURL
  }
  
  public func endpointURL(_ resourcePath: String) -> URL {
    return baseURL.appendingPathComponent("\(resourcePath)")
  }

  func httpRequestHeaders(
    withAuth: Bool = true,
    contentType: HTTPRequestHeaderContentType = .json
  ) -> [String: String] {
    var headers = [
      "Content-Type": contentType.rawValue
    ]
    if contentType == .json {
      headers["Accept"] = "application/json"
    }
    if withAuth && accessToken != nil {
      headers["Authorization"] = "Bearer \(accessToken!)"
    }

    return headers
  }
}

extension APIClient {
  var accessToken: String? {
    set { App.valet.setString(newValue, forKey: "keys:access_token") }
    get { return App.valet.getString(forKey: "keys:access_token") }
  }
  
  func request(
    _ resourcePath: String,
    method: HTTPMethod = .get,
    parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default,
    headers: HTTPHeaders? = nil,
    success: @escaping (APIResponse) -> Void,
    failure: @escaping (Error) -> Void
  ) -> DataRequest {
    return Alamofire
      .request(
        endpointURL(resourcePath),
        method: method,
        parameters: parameters,
        encoding: encoding,
        headers: headers ?? httpRequestHeaders(withAuth: true)
      )
      .apiResponse(completion: { result in
        switch result {
        case let .success(resp):
          success(resp)
        case let .failure(error):
          failure(error)
        }
      })
  }
}

// MARK: - Alamofire.DataRequest

extension DataRequest {
  @discardableResult
  func apiResponse(
    queue: DispatchQueue? = nil,
    completion: @escaping APIClientResultClosure
  ) -> DataRequest {
    return responseData(queue: queue, completionHandler: { (response: DataResponse<Data>) in

      guard response.result.error == nil else {
        App.shared.recordError(response.result.error!)
        return completion(.failure(response.result.error!))
      }

      guard let responseData = response.value else {
        return completion(.failure(APIClientError.dataNotFound(Data.self)))
      }

      do {
        let resp = try JSONDecoder().decode(APIResponse.self, from: self.utf8Data(from: responseData))

        if let code = HTTPStatusCode(rawValue: response.response!.statusCode),
          code.isRequestError || code.isServerError {
          let info = APIClientFailedRequestInfo(
            status: code,
            message: resp.message ?? S.generalErrorLabelsTypeUnknown(),
            errorCode: resp.errorCode
          )
          completion(.failure(APIClientError.failedRequest(info)))

        } else {
          completion(.success(resp))
        }

      } catch {
        App.shared.recordError(error)
        completion(.failure(error))
      }
    })
  }

  // TODO: Throw error
  private func utf8Data(from data: Data) -> Data {
    let encoding = detectEncoding(of: data)
    guard encoding != .utf8 else { return data }
    guard let responseString = String(data: data, encoding: encoding) else {
      preconditionFailure("Could not convert data to string with encoding \(encoding.rawValue)")
    }
    guard let utf8Data = responseString.data(using: .utf8) else {
      preconditionFailure("Could not convert data to UTF-8 format")
    }
    return utf8Data
  }

  private func detectEncoding(of data: Data) -> String.Encoding {
    var convertedString: NSString?
    let encoding = NSString.stringEncoding(
      for: data,
      encodingOptions: nil,
      convertedString: &convertedString,
      usedLossyConversion: nil
    )
    debugLog("~~> \(encoding)")
    return String.Encoding(rawValue: encoding)
  }
}
