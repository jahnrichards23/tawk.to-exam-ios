//
//  APIClient+Endpoints.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 OrganizationName. All rights reserved.
//

import UIKit

public struct Endpoint {

  
  // MARK: - Users
  static let usersList = "users"
  static let userInfo = "users/login"
 
}

