//
//  AppService.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

protocol AppService: AnyObject {
}

extension AppService {
  var app: App {
    return App.shared
  }
}
