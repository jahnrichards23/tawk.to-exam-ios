//
//  ModelTrackingService.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

protocol TrackableModel: Model, Identifiable {
  var isTracking: Bool { get }
}

enum ModelTrackingState {
  case tracking
  case tracked
  case untracking
  case untracked
}

protocol ModelTrackingService: AppService {

  func isTrackingModel(with id: String) -> Bool

  func addModel<T>(_ model: T) where T: TrackableModel

  func addModels<T>(_ models: [T]) where T: TrackableModel


  func removeModel(with id: String)


  func removeAllModels()

  func toggleModelTrackingState(with id: String)
}
