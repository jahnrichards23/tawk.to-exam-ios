//
//  RemoteNotificationManager.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import UIKit
import UserNotifications

class RemoteNotificationManager: AppService {
  private var userInfo: [AnyHashable: Any]?

  init() {
    setupNotificationObservers()
  }

  private func setupNotificationObservers() {
    debugLog("")
  }
}

// MARK: - General Setup

extension RemoteNotificationManager {
  var userNotificationAuthorizationOptions: UNAuthorizationOptions {
    return UNAuthorizationOptions(arrayLiteral: [.alert, .badge, .sound])
  }

  func getAuthorizationStatus(_ handler: @escaping (UNAuthorizationStatus) -> Void) {
    UNUserNotificationCenter.current().getNotificationSettings { settings in
      handler(settings.authorizationStatus)
    }
  }

  func requestPermissionToShowPushNotifications() {

    debugLog("")

    UNUserNotificationCenter.current().requestAuthorization(
      options: userNotificationAuthorizationOptions
    ) { [weak self] granted, _ in

      debugLog("Permission granted: \(granted)")
      guard granted else { return }
      self?.registerForRemoteNotificationsIfAuthorized()
    }
  }

  func registerForRemoteNotificationsIfAuthorized() {
    debugLog("")
    UNUserNotificationCenter.current().getNotificationSettings { settings in
      debugLog("Notification settings: \(settings)")
      guard settings.authorizationStatus == .authorized else { return }
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    }
  }
}

// MARK: - Notification Handling

extension RemoteNotificationManager {

  func storeNotification(_ userInfo: [AnyHashable: Any]) {
    debugLog(String(describing: userInfo))
    self.userInfo = userInfo
  }

  @discardableResult
  func processStoredNotification() -> Bool {
    guard let info = self.userInfo else {
      return false
    }
    guard let aps = info["aps"] as? JSONDictionary else {
      return false
    }
    debugLog(String(describing: aps))

    // Make sure to clear this data so we won't accidentally re-process it.
    userInfo = nil

    let application = UIApplication.shared

    if application.applicationState == .active {
      // Application is in foreground when remote notification arrived.
    } else if application.applicationState == .background || application.applicationState == .inactive {
      //
    }

    return true
  }
}
