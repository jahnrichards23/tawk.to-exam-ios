//
//  FormButton.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

@IBDesignable
class FormButton: UIButton {
  @IBInspectable var cornerRadius: CGFloat = 8 {
    didSet {
      layer.cornerRadius = cornerRadius
    }
  }

  @IBInspectable var borderColour: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3153360445) {
    didSet {
      layer.borderColor = borderColour.cgColor
    }
  }

  @IBInspectable var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    afterInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    afterInit()
  }

  func afterInit() {
    backgroundColor = Styles.Colors.primaryColor
    setTitleColor(UIColor.white, for: .normal)
    titleLabel?.font = .systemFont(ofSize: 16, weight: .medium)
  }
}
