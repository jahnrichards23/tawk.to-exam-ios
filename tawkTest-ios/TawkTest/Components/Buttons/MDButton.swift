//
//  MDButton.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import ChameleonFramework
import Material
import UIKit

class MDButton: Button {
}

class MDPrimaryButton: MDButton {
  override func prepare() {
    super.prepare()

    cornerRadiusPreset = .cornerRadius8

    backgroundColor = UIColor(
      gradientStyle: .leftToRight,
      withFrame: bounds,
      andColors: Styles.Colors.Form.Button.primaryGradient
    )
  }
}
