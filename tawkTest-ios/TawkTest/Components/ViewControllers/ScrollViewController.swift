//
//  ScrollViewController.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import PureLayout
import UIKit

class ScrollViewController: ViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var contentView: UIView!
  
  var prefersHidingBarsOnSwipe: Bool {
    return false
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    if contentView.superview == nil {
      scrollView.addSubview(contentView)
      contentView.autoPinEdgesToSuperviewEdges()
      contentView.autoMatch(.width, to: .width, of: view)
    }
    if scrollView.superview == nil {
      view.addSubview(scrollView)
      scrollView.autoPinEdgesToSuperviewEdges()
    }

    scrollView.alwaysBounceVertical = true
    scrollView.keyboardDismissMode = .interactive
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if prefersHidingBarsOnSwipe {
      navigationController?.hidesBarsOnSwipe = true
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    if prefersHidingBarsOnSwipe {
      navigationController?.hidesBarsOnSwipe = false
    }
  }
}
