//
//  ViewController.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  enum CloseButtonPosition {
    case left
    case right
  }

  var preferredCloseButtonPosition: CloseButtonPosition {
    return .left
  }

  var viewWillAppearCallCount: Int {
    return _viewWillAppearCallCount
  }

  private var _viewWillAppearCallCount = 0

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = Styles.Colors.viewControllerBackground
    setupNavBarItems()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    _viewWillAppearCallCount += 1
  }

  func setupNavBarItems() {
    if navigationController?.viewControllers.first != self {
      navigationItem.leftBarButtonItem = UIBarButtonItem(
        image: R.image.arrowLeftBlack(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
    } else if isPresentedModally {
      let button = UIBarButtonItem(
        title: S.close(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
      if preferredCloseButtonPosition == .left {
        navigationItem.leftBarButtonItem = button
      } else {
        navigationItem.rightBarButtonItem = button
      }
    }
  }

  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    if navigationController?.viewControllers.first != self {
      navigationController?.popViewController(animated: true)
    } else if isPresentedModally {
      dismiss(animated: true, completion: nil)
    }
  }
}
