//
//  TableViewController.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

class TableViewController: ViewController, UITableViewDelegate, UITableViewDataSource {
  @IBOutlet weak var tableView: UITableView!

  var clearsSelectionOnViewWillAppear = true

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    if clearsSelectionOnViewWillAppear {
      if let rows = tableView.indexPathsForSelectedRows {
        rows.forEach { indexPath in
          tableView.deselectRow(at: indexPath, animated: true)
        }
      }
    }
  }

  // MARK: UITableViewDataSource

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    fatalError("tableView(_:numberOfRowsInSection:) is not implemented")
  }

  // -- Row display

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    fatalError("tableView(_:cellForRowAt:) is not implemented")
  }

  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return nil
  }

  func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
    return nil
  }

  // -- Editing

  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return false
  }

  // -- Moving/reordering

  func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
    return false
  }

  // -- Index

  func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    return nil
  }

  func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
    return NSNotFound
  }

  // -- Data manipulation - insert and delete support

  func tableView(
    _ tableView: UITableView,
    commit editingStyle: UITableViewCell.EditingStyle,
    forRowAt indexPath: IndexPath
  ) {
    //
  }

  // -- Data manipulation - reorder / moving support

  func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    //
  }

  // MARK: UITableViewDataSource
}
