//
//  BaseView.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

class BaseView: UIView {
  override init(frame: CGRect) {
    super.init(frame: frame)
    prepare()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)

  }

  override func awakeFromNib() {
    super.awakeFromNib()
    prepare()
  }

  func prepare() {
    // Do additional setups here.
  }
}

class PartiallyTouchableView: BaseView {
  @IBOutlet var touchableViews: [UIView]?

  override func prepare() {
    super.prepare()

    backgroundColor = .clear
  }

  override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
    guard let view = super.hitTest(point, with: event) else { return nil }
    guard let touchableViews = touchableViews, touchableViews.contains(view) else { return nil }
    return view
  }
}
