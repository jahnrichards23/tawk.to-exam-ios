//
//  ImagePickerPresenter.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import MobileCoreServices
import UIKit

protocol ImagePickerPresenter {
  var imagePicker: UIImagePickerController { get set }
}

extension ImagePickerPresenter where Self: UIViewController {
  func presentPickerOptionsSheet(_ includeVideosIfPossible: Bool = false) {
    let isCameraSupported = UIImagePickerController.isSourceTypeAvailable(.camera)
    if isCameraSupported {
      let sheet = imagePickerActionsheet(includeVideosIfPossible, isCameraSupported: isCameraSupported)
      present(sheet, animated: true, completion: nil)
    } else {
      presentPhotoLibrary(includeVideosIfPossible)
    }
  }

  func presentPhotoLibrary(_ includeVideosIfPossible: Bool = false) {
    configurePickerForPhotoLibrary(includeVideosIfPossible)
    presentImagePicker()
  }

  func presentPhotoCaptureScreen() {
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
      configurePickerForPhotoCapture()
      presentImagePicker()
    } else {
      debugLog("Warning: Device doesn't have camera.")
    }
  }

  func presentVideoCaptureScreen() {
    if UIImagePickerController.isSourceTypeAvailable(.camera) {
      configurePickerForVideoCapture()
      presentImagePicker()
    } else {
      debugLog("Warning: Device doesn't have camera.")
    }
  }

  func presentImagePicker() {
    present(imagePicker, animated: true, completion: {
      self.imagePicker.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
    })
  }

  private func imagePickerActionsheet(
    _ includeVideosIfPossible: Bool,
    isCameraSupported: Bool
  ) -> UIAlertController {
    let style: UIAlertController.Style = UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet
    let menu = UIAlertController(title: nil, message: nil, preferredStyle: style)

    menu.addAction(UIAlertAction(title: S.cancel(), style: .cancel, handler: nil))

    if isCameraSupported {
      menu.addAction(
        UIAlertAction(title: S.imagePickerPresenterLabelsTakePhoto(), style: .default) { (_) -> Void in
          self.configurePickerForPhotoCapture()
          self.presentImagePicker()
      })
    }

    if includeVideosIfPossible {
      menu.addAction(
        UIAlertAction(title: S.imagePickerPresenterLabelsTakeVideo(), style: .default) { (_) -> Void in
          self.configurePickerForVideoCapture()
          self.presentImagePicker()
      })
    }

    menu.addAction(
      UIAlertAction(title: S.imagePickerPresenterLabelsChooseFromLibrary(), style: .default) { (_) -> Void in
        self.configurePickerForPhotoLibrary(includeVideosIfPossible)
        self.presentImagePicker()
    })

    return menu
  }

  private func configurePickerForVideoCapture() {
    imagePicker.sourceType = .camera
    imagePicker.cameraDevice = .rear
    imagePicker.mediaTypes = [kUTTypeMovie as String]
    imagePicker.cameraCaptureMode = .video
    imagePicker.allowsEditing = true
    imagePicker.videoQuality = .typeHigh
  }

  private func configurePickerForPhotoCapture() {
    imagePicker.sourceType = .camera
    imagePicker.cameraDevice = .rear
    imagePicker.allowsEditing = true
  }

  private func configurePickerForPhotoLibrary(_ includeVideosIfPossible: Bool = false) {
    imagePicker.sourceType = .photoLibrary
    imagePicker.allowsEditing = true

    if includeVideosIfPossible {
      imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
      imagePicker.videoQuality = .typeMedium
    } else {
      imagePicker.mediaTypes = [kUTTypeImage as String]
    }
  }
}
