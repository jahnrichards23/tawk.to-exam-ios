//
//  KeyboardEditable.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Keyboard

protocol KeyboardEditable {
  func keyboardWillShowOrHideHandler(_ notification: Notification)
}

extension KeyboardEditable where Self: UIResponder {

  func addKeyboardVisibilityEventObservers() {
    let nc = NotificationCenter.default
    _ = nc.addObserver(
      forName: UIResponder.keyboardWillShowNotification,
      object: nil,
      queue: OperationQueue.main) { [weak self] notif in
      self?.keyboardWillShowOrHideHandler(notif)
    }
    _ = nc.addObserver(
      forName: UIResponder.keyboardWillHideNotification,
      object: nil,
      queue: OperationQueue.main) { [weak self] notif in
      self?.keyboardWillShowOrHideHandler(notif)
    }
  }
}
