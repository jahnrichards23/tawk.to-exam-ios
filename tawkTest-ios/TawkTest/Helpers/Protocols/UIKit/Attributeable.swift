//
//  Attributeable.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

protocol Attributeable where Self: UIView {
  associatedtype AttributeType: ViewAttributeType
  func applyAttribute(_ attribute: AttributeType)
}

protocol ViewAttributeType {
  associatedtype ViewType: UIView
  func apply(to view: ViewType)
}
