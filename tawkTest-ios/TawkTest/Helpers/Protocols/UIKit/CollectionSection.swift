//
//  CollectionSection.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import Foundation

/// This lets you maintain an array of items along with header and footer titles.
public protocol CollectionSection {
  associatedtype ItemType

  var items: [ItemType] { get set }

  var headerTitle: String? { get set }
  var footerTitle: String? { get set }
}

open class GenericCollectionSection<T>: CollectionSection {
  public typealias ItemType = T
  open var items: [T] = []

  open var headerTitle: String?
  open var footerTitle: String?

  public init() {}
}
