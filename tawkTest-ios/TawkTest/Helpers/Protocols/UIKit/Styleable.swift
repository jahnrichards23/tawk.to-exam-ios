//
//  Styleable.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

protocol Styleable where Self: UIView {
  associatedtype StyleType: ViewStyleType
  func applyStyle(_ style: StyleType)
}

protocol ViewStyleType {
  associatedtype ViewType: UIView
  func apply(to view: ViewType)
}
