//
//  Styles.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

struct Styles {
  struct Colors {}
}

extension Styles.Colors {
  static let primaryColor = R.color.blue_665EFF()!
  static let secondaryColor = R.color.blue_3497FD()!
  static let accentColor = R.color.blue_3497FD()!

  static let textColorLight = UIColor.black // for light colored backgrounds
  static let textColorDark = UIColor.white // for dark colored backgrounds

  static let primaryTextColor = Self.textColorLight
  static let secondaryTextColor = R.color.grey_78849E()!
  static let linkTextColor = Self.primaryColor

  static let windowTint = Self.primaryColor
  static let windowBackground = UIColor.white
  static let viewControllerBackground = UIColor.white

  /// Colors for Form elements like Button, TextField, etc.
  struct Form {}
}

extension Styles.Colors.Form {
  static let normalColor = R.color.grey_78849E()!
  static let focusedColor = Styles.Colors.secondaryColor
  static let errorColor = R.color.red_FF404E()!

  static let placeholderTextColor = R.color.grey_78849E()!

  struct Button {}
}

extension Styles.Colors.Form.Button {
  static let primaryGradient = [
    R.color.blue_3ACCE1()!,
    R.color.blue_665EFF()!
  ]
}
