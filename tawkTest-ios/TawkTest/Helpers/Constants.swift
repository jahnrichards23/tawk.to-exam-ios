//
//  Constants.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String: Any]

// swiftlint:disable:next type_name
typealias S = R.string.localizable

struct Constants {
  struct Formatters {
    static let debugConsoleDateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
      formatter.timeZone = TimeZone(identifier: "UTC")!
      return formatter
    }()
  }
}
