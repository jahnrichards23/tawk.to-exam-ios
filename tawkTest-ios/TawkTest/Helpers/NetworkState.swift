//
//  NetworkState.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit
import Alamofire

struct NetworkState {
    static let shared = NetworkState()
    var isInternetAvailable: Bool {
      guard let networkChecker = NetworkReachabilityManager() else { return false }
      return networkChecker.isReachable
    }
}
