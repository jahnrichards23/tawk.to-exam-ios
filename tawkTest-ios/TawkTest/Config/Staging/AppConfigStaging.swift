//
//  AppConfigStaging.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

struct AppConfigStaging: AppConfigType {
  var baseUrl: String { "https://api.github.com" }
}
