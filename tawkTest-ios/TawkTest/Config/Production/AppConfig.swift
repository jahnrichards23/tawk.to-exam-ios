//
//  AppConfig.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

protocol AppConfigType {
  var baseUrl: String { get }
}

struct AppConfig: AppConfigType {
  var baseUrl: String { "​https://api.github.com" }
}
