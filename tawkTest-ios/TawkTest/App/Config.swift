//
//  Config.swift
//  TawkTest
//
//  Created by Richard John Alamer on 12/07/2020.
//  Copyright © 2020 OrganizationName. All rights reserved.
//

import UIKit

enum ConfigType {
  case baseurl
}

protocol Configurable {
  var type: ConfigType { get set }
  var key: String { get }
}

struct Config: Configurable {
  var type: ConfigType
  
  // TODO: - Should be environment dependent
  var key: String {
    switch type {
    case .baseurl:
      return "https://api.github.com"
    }
  }
}

