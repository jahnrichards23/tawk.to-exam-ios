//
//  AppDelegate.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import AlamofireNetworkActivityIndicator
import AlamofireNetworkActivityLogger
import IQKeyboardManagerSwift
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?

  override init() {}

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
  ) -> Bool {
    if !UIApplication.isRunningTests {
    }

    App.shared.bootstrap(with: application, launchOptions: launchOptions)

    #if DEBUG
      NetworkActivityLogger.shared.level = .debug
      NetworkActivityLogger.shared.startLogging()
    #endif

    IQKeyboardManager.shared.enable = true

    NetworkActivityIndicatorManager.shared.isEnabled = true

    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = Styles.Colors.windowBackground
    window?.tintColor = Styles.Colors.windowTint
    updateRootViewController()
    loadAppearancePreferences()
    window?.makeKeyAndVisible()

    if let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
      App.shared.remoteNotificationManager.storeNotification(userInfo)
    }

    return true
  }

  func applicationDidBecomeActive(_ application: UIApplication) {

    application.applicationIconBadgeNumber = 0
  }

  func applicationDidEnterBackground(_ application: UIApplication) {}

  func applicationWillResignActive(_ application: UIApplication) {}

  func applicationWillEnterForeground(_ application: UIApplication) {}

  func applicationWillTerminate(_ application: UIApplication) {}
}

// MARK: - Inter-App Communication

extension AppDelegate {
  func application(
    _ app: UIApplication,
    open url: URL,
    options: [UIApplication.OpenURLOptionsKey: Any] = [:]
  ) -> Bool {

    return false
  }
}
