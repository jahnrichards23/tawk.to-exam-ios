//
//  AppDelegate+Appearance.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import SVProgressHUD
import UIKit

// All UIAppearance configurations should all go in here.

// MARK: - Appearance

extension AppDelegate {
  func loadAppearancePreferences() {
    loadNavBarAppearancePrefs()
    loadBarButtonItemAppearancePrefs()
    loadTableViewCellAppearancePrefs()

    applySVProgressHUDTheme()
  }

  private func loadNavBarAppearancePrefs() {
    let navBar = UINavigationBar.appearance(whenContainedInInstancesOf: [
      NavigationController.self
    ])

    navBar.barTintColor = .white
    navBar.tintColor = .black
    navBar.titleTextAttributes = [
      NSAttributedString.Key.foregroundColor: UIColor.black as Any,
      NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .semibold) as Any
    ]
    navBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
    navBar.shadowImage = UIImage() // No shadow
    navBar.isTranslucent = false
  }

  private func loadBarButtonItemAppearancePrefs() {
    /*
     let barButton = UIBarButtonItem.appearance(whenContainedInInstancesOf: [
       ...
     ])
     barButton.setTitleTextAttributes([
         ...
       ],
       for: .normal
     )
     barButton.setTitleTextAttributes([
         ...
       ],
       for: .selected
     )
     */
  }

  private func loadTableViewCellAppearancePrefs() {
    /*
     let cell = UITableViewCell.appearance(whenContainedInInstancesOf: [...])
     cell.tintColor = ...
     */
  }
}

// MARK: - SVProgressHUD

private extension AppDelegate {
  func applySVProgressHUDTheme() {
    SVProgressHUD.setDefaultAnimationType(.native)
    SVProgressHUD.setMinimumSize(CGSize(width: 86, height: 86))

    SVProgressHUD.setDefaultStyle(.custom)
    SVProgressHUD.setBackgroundColor(R.color.grey_F7F7F7()!)
    SVProgressHUD.setBackgroundLayerColor(R.color.grey_F7F7F7()!)
  }
}
