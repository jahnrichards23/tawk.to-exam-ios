//
//  AppDelegate+RootViewController.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import NSObject_Rx
import RxSwift
import UIKit


extension AppDelegate {
  func updateRootViewController() {
      switchToDashboard()
  }

  func switchToDashboard() {
      self.window?.setRootViewControllerAnimated(R.storyboard.users.usersViewController())
  }
  
}


