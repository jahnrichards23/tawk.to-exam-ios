//
//  App.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit
import Valet

class App {
  enum Environment: String {
    case staging
    case production
  }

  static let shared = App()

  static var environment: Environment {
    // These values are set in their corresponding Targets.
    // See: Target > Build Settings > Other Swift Flags.
    #if STAGING
      return .staging
    #else
      return .production
    #endif
  }

  static let valet = Valet.valet(
    with: Identifier(nonEmpty: App.bundleIdentifier!)!,
    accessibility: .whenUnlocked
  )

  private(set) var config: AppConfigType!
  private(set) var api: APIClient!
//  private(set) var session: SessionService!
  private(set) var remoteNotificationManager: RemoteNotificationManager!

  // MARK: Initialization

  init() {
    debugLog("env: \(App.environment.rawValue)")
  }

  func bootstrap(
    with application: UIApplication,
    launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) {
//    session = SessionService()
    config = (App.environment == .production) ? AppConfig() : AppConfigStaging()
//    api = APIClient(baseURL: URL(string: config.baseUrl)!)
    api = APIClient(baseURL: URL(string: Config(type: .baseurl).key)!)
    remoteNotificationManager = RemoteNotificationManager()
  }

  func recordError(_ error: Error, info: [String: Any]? = nil) {
    debugLog(String(describing: error))

    if info != nil {
      debugLog("other info: \(String(describing: info!))")
    }

  }
}

// MARK: - App Info

extension App {
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }

  static var info: [String: Any] {
    return Bundle.main.infoDictionary ?? [:]
  }

  static var displayName: String {
    return (info["CFBundleDisplayName"] as? String) ?? "TawkTest"
  }

  static var releaseVersion: String {
    return (info["CFBundleShortVersionString"] as? String) ?? "1.0"
  }
  
  static var buildNumber: String {
    return (info["CFBundleVersion"] as? String) ?? "1"
  }
}

enum AppError: Error {
  case unauthorized(_ reason: String)
  case unknown
}

extension AppError: LocalizedError {
  var errorDescription: String? {
    switch self {
    default:
      return S.errorDevSomethingWrong()
    }
  }

  var failureReason: String? {
    switch self {
    case let .unauthorized(reason):
      return reason
    default:
      return S.errorDevUnknown()
    }
  }
}
