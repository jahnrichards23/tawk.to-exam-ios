//
//  FeedsCollectionViewCell.swift
//  StickyBeek
//
//  Created by Richard John Alamer on 12/2/19.
//  Copyright © 2020 TawkTest All rights reserved.
//

import UIKit

class UsersCollectionViewCell: UICollectionViewCell {
  
  // MARK: Properties
  
  @IBOutlet weak var imgUser: UIImageView!
  @IBOutlet weak var lblUserTitle: UILabel!
  @IBOutlet weak var lblUserDescription: UILabel!
  
  func configureWith(users: Users) {
    if let userUrl = URL(string: users.avatarURL ) {
      imgUser.setImageWithURL(userUrl)
    }
    lblUserTitle.text = users.login
//    lblUserDescription.text = users.
    

        return
  }
}
