//
//  HomeViewModel.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright (c) 2020 TawkTest. All rights reserved.
//

import NSObject_Rx
import RxSwift

protocol HomeViewModelType: AnyObject {}

extension HomeViewModelType {}

class HomeViewModel: HomeViewModelType, HasDisposeBag {
  init() {}
}
