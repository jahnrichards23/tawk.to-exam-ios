//
//  HomeController.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright (c) 2020 TawkTest. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

class HomeController: UIViewController {
  @IBOutlet private var welcomeLabel: UILabel!


  var viewModel: HomeViewModelType!
  
}

// MARK: - View LifeCycle

extension HomeController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setupView()
    setupBindings()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
}

// MARK: - View Setups & Bindings

private extension HomeController {
  func setupView() {
  }

  func setupBindings() {
    
  }
}
