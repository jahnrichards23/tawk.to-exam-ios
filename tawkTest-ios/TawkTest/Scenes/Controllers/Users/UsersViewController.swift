//
//  UsersViewController.swift
//  TawkTest
//
//  Created by Richard John Alamer on 10/07/2020.
//  Copyright © 2020 TawkTest. All rights reserved.
//

import CoreLocation
import UIKit

import RxCocoa
import RxSwift
import SVProgressHUD


class UsersViewController: UIViewController {
  
  @IBOutlet private var userViewFeeds: UICollectionView!
  @IBOutlet private var collectionViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet private var collectionViewOptions: UICollectionView!
  @IBOutlet private var lblWhatsNew: UILabel!
  @IBOutlet private var locationLabel: UILabel!
  @IBOutlet private var searchButton: UIButton!
  @IBOutlet private var viewHeightConstraint: NSLayoutConstraint!
  
  let usersViewModel = UsersViewModel()
  
  private var categoryId = ""
  private let disposeBag: DisposeBag = DisposeBag()
  private let FEEDS_HEIGHT = 390
  private var isResetCategorySelection = true
  private var isLocationLoaded = false
  private var users: BehaviorRelay<[Users]> = BehaviorRelay(value: [])
}

// MARK: - Lifecycle

extension UsersViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()

      registerCells()
      setupView()
      
      fetchUsers(isLoadMore: false)
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    title = ""
  }
}

// MARK: - Setup

private extension UsersViewController {
  
  private func setupView() {
      setupUsers()
    
  }
  
  func registerCells() {
    let feedsNib = UINib(resource: R.nib.usersCollectionViewCell)
    userViewFeeds.register(feedsNib,
                                 forCellWithReuseIdentifier: R.nib.usersCollectionViewCell.identifier)
  }

  func setupUsers() {
    users.bind(to: userViewFeeds
      .rx
      .items(cellIdentifier: R.nib.usersCollectionViewCell.identifier,
             cellType: UsersCollectionViewCell.self)) { _, users, cell in
              cell.configureWith(users: users)
    }
    .disposed(by: disposeBag)

    userViewFeeds
      .rx
      .modelSelected(Users.self)
      .subscribe(onNext: { [unowned self] users in
        self.goToUserDetail(user: users)
      })
      .disposed(by: disposeBag)
  }
}


// MARK: - Methods

private extension UsersViewController {
  
  private func hideCDefaultControls() {
    [
      locationLabel,
      searchButton,
      userViewFeeds,
      lblWhatsNew
      ]
      .forEach { $0?.isHidden = !isLocationLoaded }
    
  }
  
  func fetchUsers(isLoadMore: Bool) {

      SVProgressHUD.show()
      usersViewModel.fetchUsers(isLoadMore: isLoadMore) { [weak self] (users, error) in
                                            SVProgressHUD.dismiss()

                                            guard let myself = self, let users = users else {
                                              SVProgressHUD.showError(withStatus: error?.localizedDescription)
                                              return
                                            }

                                            if users.isEmpty {
                                              myself.users.accept([])
                                              myself.userViewFeeds.configureEmptyState(
                                                title: "Error",
                                                message: "No users available"
                                              )
                                              return
                                            }

                                            myself.userViewFeeds.removeEmptyState()

                                            let collectionViewHeight = CGFloat((myself.FEEDS_HEIGHT) * users.count)
                                            myself.collectionViewHeightConstraint.constant = collectionViewHeight
                                            myself.viewHeightConstraint.constant =  collectionViewHeight + 320
                                            myself.users.accept(users)
    }
  }
  
  private func goToUserDetail(user: Users) {
    guard let userDetailVC = R.storyboard.usersDetail.usersDetailViewController() else { return }
      present(userDetailVC, animated: true)
  }
}

// MARK: - Actions

private extension UsersViewController {
  
}

// MARK: - UICollectionViewDelegateFlowLayout

extension UsersViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    if collectionView == userViewFeeds {
      return CGSize(width: collectionView.frame.width - 10, height: 380)
    }
    return CGSize(width: 70, height: 103)
  }
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell,
                      forItemAt indexPath: IndexPath) {
    if indexPath.row == self.usersViewModel.users.count - 1 &&
      usersViewModel.currentPage < usersViewModel.lastPage {
      fetchUsers(isLoadMore: true)
    }
  }
}
