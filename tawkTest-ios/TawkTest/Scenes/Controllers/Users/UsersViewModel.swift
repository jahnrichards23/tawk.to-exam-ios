//
//  UsersViewModel.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit
import CoreLocation

class UsersViewModel {
  private let api: APIClient
    var users: [Users] = []
    var currentPage = 1
    var lastPage = 1
    private var limit = 10

    init(api: APIClient = App.shared.api) {
      self.api = api
    }

    func fetchUsers(isLoadMore: Bool, completion: @escaping ([Users]?, Error?) -> Void) {
      guard NetworkState.shared.isInternetAvailable else {
        return completion(fetchCacheUsers(), nil)
      }

      if !isLoadMore {
        currentPage = 1
      } else {
        if currentPage < lastPage {
          currentPage += 1
        } else {
          currentPage = lastPage
        }
      }

      api.getUsers() { [weak self] feedItems, error in
        guard let s = self else {
          return completion(self?.fetchCacheUsers(), nil)
        }

        if let err = error {
          return completion(s.fetchCacheUsers(), nil)
        }

        guard let feedItems = feedItems else {
          return completion(s.fetchCacheUsers(), nil)
        }

//        s.currentPage = feedItems.meta.currentPage
//        s.lastPage = feedItems.meta.lastPage
//        s.limit = feedItems.meta.perPage
//
//        if isLoadMore {
//          s.users += feedItems.data
//        } else {
//          s.users = feedItems.data
//        }

        let uniqueUsers = s.users.unique { $0.id }
        PersistenceManager.shared.saveObject(data: uniqueUsers)
        completion(uniqueUsers, nil)
      }
    }

    private func fetchCacheUsers() -> [Users] {
     
        return PersistenceManager.shared.fetchCaches(object: Users.self)
      
    }
  }
