//
//  UIImageView+URL.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Alamofire
import AlamofireImage

extension UIImageView {
  func setImageWithURL(
    _ url: URL?,
    filter: ImageFilter? = nil,
    placeholder: UIImage? = nil,
    completion: ((DataResponse<UIImage>) -> Void)? = nil
  ) {
    guard let url = url else { return }
    af_setImage(
      withURL: url,
      placeholderImage: placeholder,
      filter: filter,
      imageTransition: .crossDissolve(0.3),
      completion: { (response: DataResponse<UIImage>) in
        if let error = response.result.error {
          debugLog(error.localizedDescription)
        }
        completion?(response)
    })
  }
}
