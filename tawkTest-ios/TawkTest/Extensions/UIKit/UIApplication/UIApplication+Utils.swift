//
//  UIApplication+Utils.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import UIKit

extension UIApplication {
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }

  static var isRunningTests: Bool {
    return ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil
  }

  static func requestPermissionToShowPushNotification() {
    let app = UIApplication.shared



    let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil)

    app.registerUserNotificationSettings(settings)
    app.registerForRemoteNotifications()
  }
}
