//
//  Sequence+Unique.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import Foundation

extension Sequence where Iterator.Element: Hashable {

  func unique() -> [Iterator.Element] {
    var seen: [Iterator.Element: Bool] = [:]
    return filter { seen.updateValue(true, forKey: $0) == nil }
  }
}
