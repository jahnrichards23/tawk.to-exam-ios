//
//  Int+String.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2019 TawkTest. All rights reserved.
//

import Foundation

extension Int {
  func twoDigitString() -> String {
    return String(format: "%02d", self)
  }
}
