//
//  Array+Index.swift
//  TawkTest
//
//  Created by Richard John Alamer
//  Copyright © 2020 TawkTest. All rights reserved.
//

import Foundation

extension Array {
  subscript(safe idx: Int) -> Element? {
    return idx < endIndex ? self[idx] : nil
  }
}
