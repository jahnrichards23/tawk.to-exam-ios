//
//  Array+Extensions.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 OrganizationName. All rights reserved.
//

import UIKit

extension Array {
  func unique<T: Hashable>(map: (Element) -> (T)) -> [Element] {
    var set = Set<T>()
    var arrayOrdered = [Element]()
    for value in self {
      if !set.contains(map(value)) {
        set.insert(map(value))
        arrayOrdered.append(value)
      }
    }

    return arrayOrdered
  }
}
