//
//  UICollectionView+Extensions.swift
//  TawkTest
//
//  Created by Richard John Alamer on 11/07/2020.
//  Copyright © 2020 OrganizationName. All rights reserved.
//

import UIKit

extension UICollectionView {
  func configureEmptyState(title: String, message: String) {
    let titleLabel = UILabel()
    titleLabel.text = title
    titleLabel.numberOfLines = 0
    titleLabel.textAlignment = .center
    titleLabel.font = R.font.sfProTextSemibold(size: 19)
    titleLabel.adjustsFontSizeToFitWidth = true
    titleLabel.minimumScaleFactor = 0.5
    titleLabel.sizeToFit()
    titleLabel.widthAnchor.constraint(equalToConstant: self.frame.width).isActive = true
    
    let messageLabel = UILabel()
    messageLabel.text = message
    messageLabel.numberOfLines = 0
    messageLabel.textAlignment = .center
    messageLabel.font = R.font.sfProTextRegular(size: 17)
    messageLabel.sizeToFit()
    messageLabel.widthAnchor.constraint(equalToConstant: self.frame.width - 40).isActive = true
    
    let stackView = UIStackView()
    stackView.axis = .vertical
    stackView.distribution = .fillEqually
    stackView.alignment = .center

    stackView.addArrangedSubview(titleLabel)
    stackView.addArrangedSubview(messageLabel)
    stackView.translatesAutoresizingMaskIntoConstraints = false
    
    backgroundView = stackView
  }

  func removeEmptyState() {
    backgroundView = nil
  }
}

