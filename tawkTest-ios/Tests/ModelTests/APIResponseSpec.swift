import Quick
import Nimble
import Mockingjay

@testable import TawkTest

private struct Post: Decodable, Identifiable, Equatable {
  
  enum CodingKeys: String, CodingKey {
    case id = "post_id"
    case title
    case commentCount = "comment_count"
    case author
  }
  
  let id: String
  let title: String
  let commentCount: Int
  let author: Author
  
}

private struct Author: Decodable {
  let name: String
}

class APIResponseSpec : QuickSpec {
  
  var payload: JSONDictionary!
  
  func response(_ key: String) -> APIResponse {
    return try! DictionaryDecoder().decode(APIResponse.self, from: payload[key] as! JSONDictionary)
  }
  
  override func spec() {
    
    payload = jsonDictionaryFromFile("apiresponse")
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    describe("message") {

      context("when API request has failed", flags: [:]) {
        
        it("should not be empty", closure: {
          let resp = self.response("has_error_message")
          expect(resp).toNot(beNil())
          expect(resp.message).toNot(beEmpty())
        })
        
      }

    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    describe("errors") {

      context("when API request has failed", flags: [:]) {
        
        it("may be nil", closure: {
          let resp = self.response("has_error_message")
          expect(resp).toNot(beNil())
          expect(resp.errors).to(beNil())
        })
        
        it("may have one or more keyed messages", closure: {
          let resp = self.response("has_multiple_error_messages")
          expect(resp).toNot(beNil())
          expect(resp.errors).to(beAKindOf([String: [String]].self))
          expect(resp.errors?.count).to(beGreaterThan(0))
        })
        
      }

    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////
    describe("decodedValue") {
      
      //////////////////////////////////////////////////////////////////////////////////////////////
      context("when data attr contains a Post data", flags: [:], closure: {
        
        it("should return a Post model", closure: {
          let resp = self.response("has_post_data")
          expect(resp).toNot(beNil())
          
          let post: Post = resp.decodedValue()!
          
          expect(post.id).to(equal("xyz123"))
          expect(post.title).to(equal("Hello Earth!"))
          expect(post.author.name).to(equal("Mark"))
        })
        
      })
      
      //////////////////////////////////////////////////////////////////////////////////////////////
      context("when data attr contains a list of Post data", flags: [:], closure: {
        
        it("should return an array of Post models", closure: {
          let resp = self.response("has_posts_data")
          expect(resp).toNot(beNil())
          
          let posts: [Post]? = resp.decodedValue(forKeyPath: "posts")
          
          expect(posts).toNot(beNil())
          expect(posts!.count).to(beGreaterThanOrEqualTo(0))

          let post = posts!.first!
          expect(post.id).to(equal("xyz123"))
          expect(post.title).to(equal("Hello Earth!"))
          expect(post.author.name).to(equal("Mark"))
        })
        
      })
      
      //////////////////////////////////////////////////////////////////////////////////////////////
      context("when data attr contains an invalid JSON", flags: [:], closure: {
        
        context("and is Decodable", flags: [:], closure: {
          
          it("should return a Decodable value", closure: {
            let resp = self.response("has_string_data")
            expect(resp).toNot(beNil())
            
            let val: String? = resp.decodedValue()
            expect(val).to(beAKindOf(String.self))
            
            expect(val).to(equal("The quick brown fox jumps over the lazy dog"))
          })
          
        })
        
        context("and is non-Decodable", flags: [:], closure: {
          
          it("should return nil", closure: {
            let resp = self.response("has_null_data")
            expect(resp).toNot(beNil())
            
            let postOrNil: Post? = resp.decodedValue()
            expect(postOrNil).to(beNil())
          })
        })
        
      })
      
    }
    
  } // spec
  
}
