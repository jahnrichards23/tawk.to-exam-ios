import Quick
import TawkTest

extension QuickSpec {
  
  func jsonDictionaryFromFile(_ name: String) -> JSONDictionary {
    return TawkTest.jsonDictionaryFromFile(name, bundle: Bundle(for: type(of: self)))!
  }
  
  /// Use this for partial matching of URIs. This is because calling `stub(uri("..."))`
  /// doesn't work with partials.
  func partUri(_ uri: String) -> (_ request: URLRequest) -> Bool {
    return { (request: URLRequest) in
      return request.url?.path.hasSuffix(uri) ?? false
    }
  }
  
}
